<?php

// start the session so we can access session vars
if (session_id() == '') {
    session_start();
}

$ids = array();

// check if there is a session in the HTTP session
if (isset($_SESSION['sessionid'])) {
	$ids[] = $_SESSION['sessionid'];
}

// check if a session id was supplied in GET params
if (isset($_GET['sessionid'])) {
	$id = urlencode($_GET['sessionid']);
	if (!in_array($id, $ids))
		$ids[] = $id;
}

foreach ($ids as $id) {
	$ch = curl_init("http://192.168.250.15:8080/gateway/sessions/session/id/$id");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		// don't print output
	curl_exec($ch);
}

$_SESSION = array();

?>
