<?php
// start the session so we can access session vars
if (session_id() == '') {
    session_start();
}

// only need to create the session if one hasn't been 
// created already, or if the we're being forced to in
// the case that the session has timed out and the ID
// stored in the session is no longer valid
if (!isset($_SESSION['sessionid']) or isset($_GET['reset'])) {

    // if we are creating a new gateway session, let's 
    // try to kill any existing sessions to avoid having
    // multiple unopened sessions pertaining to the same
    // HTTP session.
    // require_once('logout.php');

    $json = '
        {
            "webAppId": "FUSIONWEBGATEWAY-D3SJN5E5SIHU",
			"urlSchemeDetails":
			{
				"host": "rp.example.com",
				"port": 8443,
				"secure": true
			},
            "presence":
            {
                "username": "%s",
                "domain": "example.com"
            }
        }
    ';

    // if supplied, use the account details provided
    $username = (empty($_GET['ps_username'])) ? 'user1' : $_GET['ps_username'];
    $json = sprintf($json, $username);
    
    // configure the curl options
    $ch = curl_init("http://192.168.250.15:8080/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json))                                
    );

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
    curl_close($ch);
    
    // decode the JSON and pick out the ID
    $decodedJson = json_decode($response);
    $id = $decodedJson->{'sessionid'};

    // store the Web Gateway session ID in the HTTP session
    $_SESSION['sessionid'] = $id;
}
?>