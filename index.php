<?php require_once('init.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Fusion Client Core SDK - Presence Sample</title>
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <h2>You are: <?php print $_GET['ps_username']; ?></h2>
        <form class='details'>
            <p>
                I am: 
                <select id='status'>
                    <option value='OFFLINE'>Offline</option>
                    <option value='AVAILABLE'>Available</option>
                    <option value='AWAY'>Away</option>
                    <option value='BUSY'>Busy</option>
                </select>
                and I'm feeling:
                <input type='text' id='status-message'>
                <input type='submit' value='Update'>
            </p>
        </form>
        <div>
            <ul id='contacts'>
                <!-- 
                This ul will contain li's whose ID will map to the 
                Contact's name property (which we will use as a
                unique identifier).
                 -->
            </ul>
        </div>
    </body>
</html>

<!-- libraries -->
<script type='text/javascript' src='http://192.168.250.15:8080/gateway/adapter.js'></script>
<script type='text/javascript' src='http://192.168.250.15:8080/gateway/fusion-client-sdk.js'></script>

<!-- setup Fusion Client SDK --> 
<script type='text/javascript'>

    // make the UI inactive until the UC is initialised
    $('input, select').attr('disabled', 'disabled');
    
    // Client SDK init code
    window.sessionID = "<?php echo $_SESSION['sessionid']; ?>";

    UC.start(sessionID, []);

    UC.onInitialised = function () {
        $('input').removeAttr('disabled');
    };

    // setup the presence handlers
    UC.presence.onOwnStatusChange = function (status, statusMessage) {
        // in case the select is disabled, enable it
        $('select').removeAttr('disabled');
        
        // update the <select id='#status'> with the value of the
        // status you've discovered through this function
        $('#status').val(status);

        // update the <input type='text' id='status-message'> with 
        // the value of the statusMessage you've discovered through this function
        // status message is optional - so check before updating UI
        if (statusMessage) {
            $('#status-message').val(statusMessage);
        }

        // if you're currently 'OFFLINE', ask the user if they want
        // to appear online
        if (status === 'OFFLINE' && confirm ('Offline - want to go online?')) {
            UC.presence.setStatus('AVAILABLE', statusMessage);
        }
    };

    // update the UI if our contacts state changes
    UC.presence.onContactStatusChange = function (contact) {
        // get the li containing the contact's state & message - and update them
        $elem = $('#contacts').find('#' + contact.name);

        if ($elem.length === 0) {
            $elem = $('<li>').attr('id', contact.name);
            $('#contacts').append($elem);
        }

        // set the values for the contact
        $elem.text(contact.name + ' : ' + contact.status + ' : ' + contact.customStatusMessage);
    };
    
    // when the user updates their details
    $('form.details').submit(function (e) {
        e.preventDefault();
        // retrieve the users' details and use the 
        // UC.presence object to update their status 
        // on the presence server
        var status = $('#status').val();
        var statusMessage = $('#status-message').val();
        UC.presence.setStatus(status, statusMessage);
    });
</script>
